#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

unsigned int _V_ = 0;
unsigned int _ECnt_ = 0;
unsigned int* _E1_ = NULL;
unsigned int* _E2_ = NULL;
unsigned int Allocated = 0;

void Free (void)
{
    if (Allocated)
    {
        free(_E1_);
        free(_E2_);
    }
    Allocated = 0;
    return;
}

int SetV (char * string)
{
    int RetVal;
    unsigned int i;
    RetVal = sscanf(string,"%u",&_V_);
    if (RetVal != 1)
        return 1;
    Free();
    _ECnt_ = 0;
    _E1_ = (unsigned int *)(malloc(_V_*(_V_ - 1)/2*sizeof(unsigned int)));
    _E2_ = (unsigned int *)(malloc(_V_*(_V_ - 1)/2*sizeof(unsigned int)));
    Allocated = 1;
    return 0;
}

int SetE (char * string, char * ErrorMsg)
{
    // Strip from { and }
    int RetVal;
    unsigned int e1;
    unsigned int e2;
    int ErrorID;
    RetVal = sscanf(string,"{%s}",string);
    if ((RetVal != 1) || (string[strlen(string) - 1] != '}'))
    {
        strcpy(ErrorMsg,"Error: Could not strip of '{' and/or '}'.\n");
        return 1;
    }
    string[strlen(string) - 1] = '\0';
    strcat(string,",-");
    _ECnt_ = 0;
    while (strlen(string) > 2)
    {
        RetVal = sscanf(string,"<%u,%u>,%s",&e1,&e2,string);
        if (RetVal == 3)
        {
            ErrorID = 0;
            if (e1 == e2)
            {
                sprintf(ErrorMsg,"Error: Element %u of E (<%u,%u>) has identical vertices.\n",
                    _ECnt_ + 1,e1,e2);
                ErrorID = 3;
            }
            if ((e1 >= _V_) || (e2 >= _V_))
            {
                sprintf(ErrorMsg,"Error: Element %u of E (<%u,%u>) is out of range.\n",
                    _ECnt_ + 1,e1,e2);
                ErrorID = 4;
            }
            if (_ECnt_ >= _V_*(_V_ - 1)/2)
            {
                sprintf(ErrorMsg,"Error: Too many edges.\n");
                ErrorID = 5;
            }
            if (ErrorID)
            {
                return ErrorID;
            }
            _E1_[_ECnt_] = e1;
            _E2_[_ECnt_] = e2;
            _ECnt_++;
        }
        else
        {
            strcpy(ErrorMsg,"Error: Format was wrong.\n");
            return 2;
        }
    }
    return 0;
}

unsigned int GetV (void)
{
    return _V_;
}

unsigned int GetECnt (void)
{
    return _ECnt_;
}

int GetGraph (unsigned int* E1, unsigned int* E2)
{
    unsigned int i;
    if (!Allocated)
        return 1;
    for (i = 0; i < _ECnt_; i++)
    {
        E1[i] = _E1_[i];
        E2[i] = _E2_[i];
    }
    return 0;
}
