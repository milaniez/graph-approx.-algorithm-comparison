clc
clear


FontName = 'Times New Roman';
FontSize = 16;
Markers = {'x','o','+','s','d','*','v','^','<','>','p','h','.'};
MarkerSize = 10;

LegendText = {  'CNF-SAT-VC','APPROX-VC-1','REFINED-APPROX-VC-1',...
                'APPROX-VC-2','REFINED-APPROX-VC-2'};

VCnt = 5:15;

ApproxRatioMean = importdata ('A5ResultsMean.txt');
ApproxRatioStd  = importdata ('A5ResultsStd.txt');
A5TimesMean     = importdata ('A5TimesMean.txt');
A5TimesStd      = importdata ('A5TimesStd.txt');

ApproxRatioMeanH = figure;
axis([4 16 (min(min(ApproxRatioMean)) - 0.05) (max(max(ApproxRatioMean)) + 0.05)]);
title('Approximation Mean','FontName',FontName,'FontSize',FontSize);
xlabel('Number of vertices','FontName',FontName,'FontSize',FontSize);
ylabel('Mean approximation ratio','FontName',FontName,'FontSize',FontSize);
hold on;
grid on;

for i = 1:5
    plot(VCnt,ApproxRatioMean(i,:),'k','Marker',Markers{i},...
        'MarkerEdgeColor','k','MarkerSize',MarkerSize);
end
LegendHandle = legend(LegendText);
set(LegendHandle,'Location','Best','FontName',FontName,'FontSize',10);
print(ApproxRatioMeanH,'-dpdf','ApproxRatioMean.pdf');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ApproxRatioStdH = figure;
axis([4 16 (min(min(ApproxRatioStd)) - 0.05) (max(max(ApproxRatioStd)) + 0.05)]);
title('Approximation Std','FontName',FontName,'FontSize',FontSize);
xlabel('Number of vertices','FontName',FontName,'FontSize',FontSize);
ylabel('Approximation ratio std','FontName',FontName,'FontSize',FontSize);
hold on;
grid on;

for i = 1:5
    plot(VCnt,ApproxRatioStd(i,:),'k','Marker',Markers{i},...
        'MarkerEdgeColor','k','MarkerSize',MarkerSize);
end
LegendHandle = legend(LegendText);
set(LegendHandle,'Location','Best','FontName',FontName,'FontSize',10);
print(ApproxRatioStdH,'-dpdf','ApproxRatioStd.pdf');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
A5TimesMeanH = figure;
% axis([4 16 (min(min(ApproxRatioStd)) - 0.05) (max(max(ApproxRatioStd)) + 0.05)]);
grid on;

for i = 1:5
    semilogy(VCnt,A5TimesMean(i,:)*1000000,'k','Marker',Markers{i},...
        'MarkerEdgeColor','k','MarkerSize',MarkerSize);
    hold on;
end
title('Mean Time','FontName',FontName,'FontSize',FontSize);
xlabel('Number of vertices','FontName',FontName,'FontSize',FontSize);
ylabel('Mean Running Time ( {\mu}s )','FontName',FontName,'FontSize',FontSize);
LegendHandle = legend(LegendText);
set(LegendHandle,'Location','Best','FontName',FontName,'FontSize',10);
print(A5TimesMeanH,'-dpdf','TimeMean.pdf');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A5TimesStdH = figure;
% axis([4 16 (min(min(ApproxRatioStd)) - 0.05) (max(max(ApproxRatioStd)) + 0.05)]);
grid on;

for i = 1:5
    semilogy(VCnt,A5TimesStd(i,:)*1000000,'k','Marker',Markers{i},...
        'MarkerEdgeColor','k','MarkerSize',MarkerSize);
    hold on;
end
title('Mean Time Std','FontName',FontName,'FontSize',FontSize);
xlabel('Number of vertices','FontName',FontName,'FontSize',FontSize);
ylabel('Mean running time ( {\mu}s )','FontName',FontName,'FontSize',FontSize);
LegendHandle = legend(LegendText);
set(LegendHandle,'Location','Best','FontName',FontName,'FontSize',10);
print(A5TimesStdH,'-dpdf','TimeStd.pdf');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close(ApproxRatioMeanH);
close(ApproxRatioStdH);
close(A5TimesMeanH);
close(A5TimesStdH);