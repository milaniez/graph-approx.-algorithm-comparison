#ifndef _MYGRAPH_INCLUDED_
#define _MYGRAPH_INCLUDED_

void Free (void);
int SetV (char * string);
int SetE (char * string, char * ErrorMsg);
unsigned int GetV (void);
unsigned int GetECnt (void);
int GetGraph (unsigned int* E1, unsigned int* E2);

#endif
