#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include "vc2cnf.h"
#include "mygraph.h"
#include "SAT.h"

#define MAX_STRING_SIZE 65536

#define DEBUGD(A) \
    do {printf("%d\n",A);fflush(stdout);} while (0)

#define DEBUGP(A) \
    do {printf("%p\n",A);fflush(stdout);} while (0)

#define DEBUGS(A) \
    do {printf("%s\n",A);fflush(stdout);} while (0)

void q_sort(int numbers[], int left, int right)
{
    int pivot, l_hold, r_hold;

    l_hold = left;
    r_hold = right;
    pivot = numbers[left];
    while (left < right)
    {
        while ((numbers[right] >= pivot) && (left < right))
            right--;
        if (left != right)
        {
            numbers[left] = numbers[right];
            left++;
        }
        while ((numbers[left] <= pivot) && (left < right))
            left++;
        if (left != right)
        {
            numbers[right] = numbers[left];
            right--;
        }
    }
    numbers[left] = pivot;
    pivot = left;
    left = l_hold;
    right = r_hold;
    if (left < pivot)
        q_sort(numbers, left, pivot-1);
    if (right > pivot)
        q_sort(numbers, pivot+1, right);
}

void quickSort(int numbers[], int array_size)
{
    q_sort(numbers, 0, array_size - 1);
}

typedef struct
{
    unsigned int V;
    unsigned int ECnt;
    unsigned int* E1;
    unsigned int* E2;
    char OutputString[MAX_STRING_SIZE];
    struct timespec TS;
} runnerArg_t;

runnerArg_t ConstructRunnerArg (unsigned int V,unsigned int ECnt,
                                unsigned int* E1,unsigned int* E2)
{
    runnerArg_t RunnerArg;
    int Idx;
    RunnerArg.V = V;
    RunnerArg.ECnt = ECnt;
    RunnerArg.E1 = (unsigned int *)(malloc(sizeof(unsigned int)*ECnt));
    RunnerArg.E2 = (unsigned int *)(malloc(sizeof(unsigned int)*ECnt));
    for (Idx = 0; Idx < ECnt; Idx++)
    {
        RunnerArg.E1[Idx] = E1[Idx];
        RunnerArg.E2[Idx] = E2[Idx];
    }
    return RunnerArg;
}

void DestructRunnerArg (runnerArg_t RunnerArg)
{
    free(RunnerArg.E1);
    free(RunnerArg.E2);
}

struct timespec GetTimeSpec(void)
{
    clockid_t CID;
    struct timespec TS;
    int int_RetVal;
    int_RetVal = pthread_getcpuclockid(pthread_self(),&CID);
    if (int_RetVal)
    {
        fprintf(stderr,"Error: in pthread_getcpuclockid.\n");
        exit(EXIT_FAILURE);
    }
    if (clock_gettime(CID,&TS) == -1)
    {
        fprintf(stderr,"Error: in clock_gettime.\n");
        exit(EXIT_FAILURE);
    }
    return TS;
}

void cnf_sat_vc(unsigned int V,unsigned int ECnt,unsigned int* E1,
                unsigned int* E2,char* OutputString)
{
    unsigned int ENo;
    
    int AtomCnt,ClauseCnt;
    int * ClauseLengthes;
    int ** Clauses;
    int ClauseNo,LiteralNo,AtomNo;
    
    int k;
    SAT_Manager mgr;
    int result;
    int * resAtoms;
    int resAtomNo;
    
    InitGraph(V,ECnt,E1,E2);
    for (k = 1; k <= V; k++)
    {
        InitVC2CNF(k);
        AtomCnt = GetAtomCnt();
        ClauseCnt = GetClauseCnt();
        Clauses = (int **)(malloc(sizeof(int*)*ClauseCnt));
        ClauseLengthes = (int *)(malloc(sizeof(int)*ClauseCnt));
        GetClauseLengthes(ClauseLengthes);
        for (ClauseNo = 0; ClauseNo < ClauseCnt; ClauseNo++)
            Clauses[ClauseNo] = (int *)((malloc(sizeof(int)*
                ClauseLengthes[ClauseNo])));
        MakeCNF(Clauses);
        
        mgr = SAT_InitManager();
        SAT_SetNumVariables(mgr, AtomCnt);
        for (ClauseNo = 0; ClauseNo < ClauseCnt; ClauseNo++)
        {
            SAT_AddClause(mgr,Clauses[ClauseNo],ClauseLengthes[ClauseNo]);
        }
        result = SAT_Solve(mgr);
        if (result ==  SATISFIABLE)
            break;
    }
    resAtomNo = 0;
    resAtoms = (int *)(malloc(sizeof(int)*k));
    for (AtomNo = 1; AtomNo <= AtomCnt; AtomNo++)
    {
        result = SAT_GetVarAsgnment(mgr,AtomNo);
        if (result == 1)
            resAtoms[resAtomNo++] = (AtomNo - 1)/k;
    }
    quickSort(resAtoms,k);
    sprintf(OutputString,"%d",resAtoms[0]);
    for (AtomNo = 1; AtomNo < k; AtomNo++)
    {
        sprintf(OutputString,"%s,%d",OutputString,resAtoms[AtomNo]);
    }
    for (ClauseNo = 0; ClauseNo < ClauseCnt; ClauseNo++)
        free(Clauses[ClauseNo]);
    free(ClauseLengthes);
    free(Clauses);
}

void* cnf_sat_vc_runner (void* inpArg)
{
    runnerArg_t* RunnerArg;
    RunnerArg = (runnerArg_t*)(inpArg);
    cnf_sat_vc( RunnerArg->V, RunnerArg->ECnt,RunnerArg->E1,
                RunnerArg->E2,RunnerArg->OutputString);
    RunnerArg->TS = GetTimeSpec();
    return NULL;
}

unsigned int GetHighestRankedVertex(unsigned int V,unsigned int ECnt,
                                    unsigned int* E1,unsigned int* E2)
{
    unsigned int* VDeg;
    int Idx;
    int MaxIdx = 0;
    VDeg = (unsigned int *)(malloc(sizeof(unsigned int)*V));
    for (Idx = 0; Idx < V; Idx++)
        VDeg[Idx] = 0;
    for (Idx = 0; Idx < ECnt; Idx++)
    {
        VDeg[E1[Idx]]++;
        VDeg[E2[Idx]]++;
    }
    for (Idx = 1; Idx < V; Idx++)
        if (VDeg[Idx] > VDeg[MaxIdx])
            MaxIdx = Idx;
    return MaxIdx;
}

void RemoveVertexFromEdges( unsigned int V2Rem, unsigned int* ECnt,
                            unsigned int* E1, unsigned int* E2)
{
    int Idx;
    for (Idx = *ECnt - 1; Idx >= 0; Idx--)
    {
        if ((E1[Idx] == V2Rem) || (E2[Idx] == V2Rem))
        {
            E1[Idx] = E1[*ECnt - 1];
            E2[Idx] = E2[*ECnt - 1];
            (*ECnt)--;
        }
    }
}

void approx_vc_1 (  unsigned int V,unsigned int ECnt,unsigned int* E1,
                    unsigned int* E2,char* OutputString, int* VCover, int* VCoverCnt)
{
    int VCoverNo;
    *VCoverCnt = 0;
    while (ECnt)
    {
        VCover[*VCoverCnt] = (int)(GetHighestRankedVertex(V,ECnt,E1,E2));
        RemoveVertexFromEdges((unsigned int)(VCover[*VCoverCnt]),&ECnt,E1,E2);
        (*VCoverCnt)++;
    }
    quickSort(VCover,*VCoverCnt);
    sprintf(OutputString,"%d",VCover[0]);
    for (VCoverNo = 1; VCoverNo < *VCoverCnt; VCoverNo++)
    {
        sprintf(OutputString,"%s,%d",OutputString,VCover[VCoverNo]);
    }
}

void* approx_vc_1_runner (void* inpArg)
{
    runnerArg_t* RunnerArg;
    int* VCover;
    int VCoverCnt;
    RunnerArg = (runnerArg_t*)(inpArg);
    VCover = (int *)(malloc(sizeof(int)*RunnerArg->ECnt));
    approx_vc_1(RunnerArg->V, RunnerArg->ECnt,RunnerArg->E1,
                RunnerArg->E2,RunnerArg->OutputString,
                VCover,&VCoverCnt);
    RunnerArg->TS = GetTimeSpec();
    return NULL;
}

void Swap (int * X, int idx1, int idx2)
{
    int tmp;
    tmp = X[idx1];
    X[idx1] = X[idx2];
    X[idx2] = tmp;
}

int IsItVertexCover(unsigned int* E1,unsigned int* E2,
                    unsigned int ECnt,int* VCover,int VCoverCnt)
{
    int VCoverNo;
    int ENo;
    for (VCoverNo = 0; VCoverNo < VCoverCnt; VCoverNo++)
    {
        for (ENo = ((int)(ECnt)) - 1; ENo >= 0 ; ENo--)
        {
            if ((E1[ENo] == VCover[VCoverNo]) || (E2[ENo] == VCover[VCoverNo]))
            {
                E1[ENo] = E1[ECnt - 1];
                E2[ENo] = E2[ECnt - 1];
                ECnt--;
            }
        }
    }
    return !ECnt;
}

void RefineVC ( unsigned int V,unsigned int ECnt,unsigned int* E1,
                unsigned int* E2,int* VCover,int* VCoverCnt)
{
    int VCoverNo;
    runnerArg_t RunnerArgCpy;
    for (VCoverNo = (*VCoverCnt) - 1; VCoverNo >= 0; VCoverNo--)
    {
        RunnerArgCpy = ConstructRunnerArg (V,ECnt,E1,E2);
        Swap(VCover,(*VCoverCnt) - 1,VCoverNo);
        if (    IsItVertexCover(RunnerArgCpy.E1,RunnerArgCpy.E2,
                RunnerArgCpy.ECnt,VCover,(*VCoverCnt) - 1))
            (*VCoverCnt)--;
        else
            Swap(VCover,(*VCoverCnt) - 1,VCoverNo);
        DestructRunnerArg(RunnerArgCpy);
    }
    quickSort(VCover,*VCoverCnt);
}

void refined_approx_vc_1 (  unsigned int V,unsigned int ECnt,unsigned int* E1,
                            unsigned int* E2,char* OutputString)
{
    runnerArg_t RunnerArgCpy;
    int* VCover;
    int VCoverCnt;
    int VCoverNo;
    RunnerArgCpy = ConstructRunnerArg (V,ECnt,E1,E2);
    VCover = (int *)(malloc(sizeof(int)*ECnt));
    approx_vc_1(RunnerArgCpy.V, RunnerArgCpy.ECnt,RunnerArgCpy.E1,
                RunnerArgCpy.E2,RunnerArgCpy.OutputString,
                VCover,&VCoverCnt);
    DestructRunnerArg(RunnerArgCpy);
    RefineVC ( V,ECnt,E1,E2,VCover,&VCoverCnt);
    sprintf(OutputString,"%d",VCover[0]);
    for (VCoverNo = 1; VCoverNo < VCoverCnt; VCoverNo++)
    {
        sprintf(OutputString,"%s,%d",OutputString,VCover[VCoverNo]);
    }
}

void* refined_approx_vc_1_runner (void* inpArg)
{
    runnerArg_t* RunnerArg;
    RunnerArg = (runnerArg_t*)(inpArg);
    refined_approx_vc_1(RunnerArg->V, RunnerArg->ECnt,RunnerArg->E1,
                        RunnerArg->E2,RunnerArg->OutputString);
    RunnerArg->TS = GetTimeSpec();
    return NULL;
}

void approx_vc_2 (  unsigned int V,unsigned int ECnt,unsigned int* E1,
                    unsigned int* E2,char* OutputString, int* VCover, int* VCoverCnt)
{
    int VCoverNo;
    *VCoverCnt = 0;
    while (ECnt)
    {
        VCover[*VCoverCnt] = (int)(E1[0]);
        VCover[(*VCoverCnt) + 1] = (int)(E2[0]);
        RemoveVertexFromEdges((unsigned int)(VCover[*VCoverCnt]),&ECnt,E1,E2);
        RemoveVertexFromEdges((unsigned int)(VCover[(*VCoverCnt) + 1]),&ECnt,E1,E2);
        (*VCoverCnt) += 2;
    }
    quickSort(VCover,*VCoverCnt);
    sprintf(OutputString,"%d",VCover[0]);
    for (VCoverNo = 1; VCoverNo < *VCoverCnt; VCoverNo++)
    {
        sprintf(OutputString,"%s,%d",OutputString,VCover[VCoverNo]);
    }
    
}

void* approx_vc_2_runner (void* inpArg)
{
    runnerArg_t* RunnerArg;
    int* VCover;
    int VCoverCnt;
    RunnerArg = (runnerArg_t*)(inpArg);
    VCover = (int *)(malloc(sizeof(int)*RunnerArg->ECnt));
    approx_vc_2(RunnerArg->V, RunnerArg->ECnt,RunnerArg->E1,
                RunnerArg->E2,RunnerArg->OutputString,
                VCover,&VCoverCnt);
    RunnerArg->TS = GetTimeSpec();
    return NULL;
}

void refined_approx_vc_2 (  unsigned int V,unsigned int ECnt,unsigned int* E1,
                            unsigned int* E2,char* OutputString)
{
    runnerArg_t RunnerArgCpy;
    int* VCover;
    int VCoverCnt;
    int VCoverNo;
    RunnerArgCpy = ConstructRunnerArg (V,ECnt,E1,E2);
    VCover = (int *)(malloc(sizeof(int)*ECnt));
    approx_vc_2(RunnerArgCpy.V, RunnerArgCpy.ECnt,RunnerArgCpy.E1,
                RunnerArgCpy.E2,RunnerArgCpy.OutputString,
                VCover,&VCoverCnt);
    DestructRunnerArg(RunnerArgCpy);
    RefineVC ( V,ECnt,E1,E2,VCover,&VCoverCnt);
    sprintf(OutputString,"%d",VCover[0]);
    for (VCoverNo = 1; VCoverNo < VCoverCnt; VCoverNo++)
    {
        sprintf(OutputString,"%s,%d",OutputString,VCover[VCoverNo]);
    }
}

void* refined_approx_vc_2_runner (void* inpArg)
{
    runnerArg_t* RunnerArg;
    RunnerArg = (runnerArg_t*)(inpArg);
    refined_approx_vc_2(RunnerArg->V, RunnerArg->ECnt,RunnerArg->E1,
                        RunnerArg->E2,RunnerArg->OutputString);
    RunnerArg->TS = GetTimeSpec();
    return NULL;
}

int main()
{
    char InputString[MAX_STRING_SIZE + 1];
    char OutputString[MAX_STRING_SIZE + 1];
    char ErrorString[MAX_STRING_SIZE + 1];
    
    unsigned int V = 0;
    unsigned int ECnt = 0;
    unsigned int* E1 = NULL;
    unsigned int* E2 = NULL;
    unsigned int Allocated = 0;
    
    char* fgets_RetVal;
    int int_RetVal;
    char Cmd[100];
    char Param1[MAX_STRING_SIZE + 1];
    char ParamX[MAX_STRING_SIZE + 1];
    
    runnerArg_t cnf_sat_vc_Arg;
    runnerArg_t approx_vc_1_Arg;
    runnerArg_t refined_approx_vc_1_Arg;
    runnerArg_t approx_vc_2_Arg;
    runnerArg_t refined_approx_vc_2_Arg;
    
    pthread_t cnf_sat_vc_ThreadID;
    pthread_t approx_vc_1_ThreadID;
    pthread_t refined_approx_vc_1_ThreadID;
    pthread_t approx_vc_2_ThreadID;
    pthread_t refined_approx_vc_2_ThreadID;
    
    while (1)
    {
        fgets_RetVal = fgets(InputString,MAX_STRING_SIZE,stdin);
        if (fgets_RetVal == NULL)
        {
            break;
        }
        int_RetVal = sscanf(InputString,"%s %s %s\n",Cmd,Param1,ParamX);
        if (int_RetVal < 1)
        {
            continue;
        }
        if (strcmp(Cmd,"V") == 0)
        {
            if (int_RetVal != 2)
            {
                fprintf(stderr,"Error: Too few/many arguments for command '%s'.\n",Cmd);
                return 1;
            }
            if (SetV(Param1))
            {
                fprintf(stderr,"Error: Not a valid Parameter for Command 'V'.\n");
                return 1;
            }
            continue;
        }
        if (strcmp(Cmd,"E") == 0)
        {
            if (int_RetVal != 2)
            {
                fprintf(stderr,"Error: Too few/many arguments for command '%s'.\n",Cmd);
                return 1;
            }
            int_RetVal = SetE(Param1,ErrorString);
            if (int_RetVal != 0)
            {
                puts(ErrorString);
                return 1;
            }
            V = GetV();
            ECnt = GetECnt();
            if (ECnt == 0)
            {
                printf("\n");
                continue;
            }
            if (Allocated)
            {
                free(E1);
                free(E2);
                Allocated = 0;
            }
            E1 = (unsigned int *)(malloc(sizeof(unsigned int)*ECnt));
            E2 = (unsigned int *)(malloc(sizeof(unsigned int)*ECnt));
            Allocated = 1;
            if (GetGraph(E1,E2))
            {
                fprintf(stderr,"Tried E before V\n");
                return 1;
            }
            // Starting the process of VC to CNF
            
            cnf_sat_vc_Arg = ConstructRunnerArg (V,ECnt,E1,E2);
            //cnf_sat_vc_runner(&cnf_sat_vc_Arg);
            pthread_create (&cnf_sat_vc_ThreadID,NULL,
                            &cnf_sat_vc_runner,
                            (void *)(&cnf_sat_vc_Arg));
            
            approx_vc_1_Arg = ConstructRunnerArg (V,ECnt,E1,E2);
            //approx_vc_1_runner(&approx_vc_1_Arg);
            pthread_create (&approx_vc_1_ThreadID,NULL,
                            &approx_vc_1_runner,
                            (void *)(&approx_vc_1_Arg));
            
            refined_approx_vc_1_Arg = ConstructRunnerArg (V,ECnt,E1,E2);
            //refined_approx_vc_1_runner(&refined_approx_vc_1_Arg);
            pthread_create (&refined_approx_vc_1_ThreadID,NULL,
                            &refined_approx_vc_1_runner,
                            (void *)(&refined_approx_vc_1_Arg));
            
            approx_vc_2_Arg = ConstructRunnerArg (V,ECnt,E1,E2);
            //approx_vc_2_runner(&approx_vc_2_Arg);
            pthread_create (&approx_vc_2_ThreadID,NULL,
                            &approx_vc_2_runner,
                            (void *)(&approx_vc_2_Arg));
            
            refined_approx_vc_2_Arg = ConstructRunnerArg (V,ECnt,E1,E2);
            //refined_approx_vc_2_runner(&refined_approx_vc_2_Arg);
            pthread_create (&refined_approx_vc_2_ThreadID,NULL,
                            &refined_approx_vc_2_runner,
                            (void *)(&refined_approx_vc_2_Arg));
            
            pthread_join(cnf_sat_vc_ThreadID,NULL);
            pthread_join(approx_vc_1_ThreadID,NULL);
            pthread_join(refined_approx_vc_1_ThreadID,NULL);
            pthread_join(approx_vc_2_ThreadID,NULL);
            pthread_join(refined_approx_vc_2_ThreadID,NULL);
            
            printf( "CNF-SAT-VC: %s\n",
                    cnf_sat_vc_Arg.OutputString);
            printf( "APPROX-VC-1: %s\n",
                    approx_vc_1_Arg.OutputString);
            printf( "REFINED-APPROX-VC-1: %s\n",
                    refined_approx_vc_1_Arg.OutputString);
            printf( "APPROX-VC-2: %s\n",
                    approx_vc_2_Arg.OutputString);
            printf( "REFINED-APPROX-VC-2: %s\n",
                    refined_approx_vc_2_Arg.OutputString);
            
            printf("CNF-SAT-VC-Time1: %ld.%09ld\n",
                    cnf_sat_vc_Arg.TS.tv_sec,
                    cnf_sat_vc_Arg.TS.tv_nsec);
            printf("APPROX-VC-1-Time2: %ld.%09ld\n",
                    approx_vc_1_Arg.TS.tv_sec,
                    approx_vc_1_Arg.TS.tv_nsec);
            printf("REFINED-APPROX-VC-1-Time3: %ld.%09ld\n",
                    refined_approx_vc_1_Arg.TS.tv_sec,
                    refined_approx_vc_1_Arg.TS.tv_nsec);
            printf("APPROX-VC-2-Time4: %ld.%09ld\n",
                    approx_vc_2_Arg.TS.tv_sec,
                    approx_vc_2_Arg.TS.tv_nsec);
            printf("REFINED-APPROX-VC-2-Time5: %ld.%09ld\n",
                    refined_approx_vc_2_Arg.TS.tv_sec,
                    refined_approx_vc_2_Arg.TS.tv_nsec);
            fflush(stdout);
            
            DestructRunnerArg(cnf_sat_vc_Arg);
            DestructRunnerArg(approx_vc_1_Arg);
            DestructRunnerArg(refined_approx_vc_1_Arg);
            DestructRunnerArg(approx_vc_2_Arg);
            DestructRunnerArg(refined_approx_vc_2_Arg);
            
            continue;
        }
        fprintf(stderr,"Error: Invalid Command, '%s'.\n",Cmd);
        return 1;
    }
    if (Allocated)
    {
        free(E1);
        free(E2);
        Allocated = 0;
    }
    return 0;
}
