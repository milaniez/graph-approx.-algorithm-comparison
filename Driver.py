#!/usr/bin/python
from __future__ import division
import shlex
from subprocess import PIPE, STDOUT, Popen

def Mean(A):
    x = 0;
    for y in A:
        x = x + y;
    x = x/len(A);
    return x;
    
def Std(A):
    m = Mean(A)
    x = 0
    for y in A:
        x = x + (y - m)*(y - m);
    x = x/len(A);
    x = x ** (0.5);
    return x;
    

MinV = 5;
MaxV = 15;
TryCnt = 10;

A5Args = ["./a5-ece650"];
graphGenLocation = "/home/wdietl/graphGen/graphGen";

A5Handler = Popen(A5Args,stdout=PIPE,stdin=PIPE);

A5Results = [];
A5Times = [];
for VCount in range(MinV,MaxV + 1):
    A5Results.append([]);
    A5Times.append([]);
    for TryNo in range(TryCnt):
        graphGenArgs = [graphGenLocation,'%(0)d' % {'0':VCount}];
        graphGenHandler = Popen(graphGenArgs,stdout=PIPE);
        (graphGenStdOutData,graphGenStdErrData) = graphGenHandler.communicate();
        while True:
            A5Handler = Popen(A5Args,stdout=PIPE,stdin=PIPE);
            (A5StdOutData,A5StdErrData) = A5Handler.communicate(graphGenStdOutData);
            #print ">>>>"
            if A5Handler.poll() == 0:
                break;
        #print ">>>>"
        #print A5Handler.poll()
        #print A5StdErrData
        #print graphGenStdOutData
        #print A5StdOutData
        A5StdOutLines = A5StdOutData.strip().split('\n');
        #print str(VCount) + " - " + str(TryNo)
        #print A5StdOutLines
        for LineNo in range(len(A5StdOutLines) - 1, -1, -1):
            A5StdOutLines[LineNo] = A5StdOutLines[LineNo].strip();
            if  A5StdOutLines[LineNo].startswith('Decision:') or \
                    len(A5StdOutLines[LineNo]) == 0:
                del A5StdOutLines[LineNo];
            else:
                A5StdOutLines[LineNo] = A5StdOutLines[LineNo].split();
        #print A5StdOutLines
        #print ""
        for LineNo in range(0,5):
            if TryNo == 0:
                A5Results[VCount - MinV].append([]);
                A5Times  [VCount - MinV].append([]);
            A5Results[VCount - MinV][LineNo].append(len(A5StdOutLines[LineNo][1].split(',')));
            A5Times  [VCount - MinV][LineNo].append(float(A5StdOutLines[LineNo + 5][1]));

for VCount in range(0,MaxV - MinV + 1):
    for AlgNo in range(4,-1,-1):
        for TryNo in range(TryCnt):
            A5Results[VCount][AlgNo][TryNo] = \
                    A5Results[VCount][AlgNo][TryNo]/ \
                    A5Results[VCount][0][TryNo];

A5ResultsMean = [];
A5ResultsStd = [];
A5TimesMean = [];
A5TimesStd = [];
for AlgNo in range(5):
    A5ResultsMean.append([]);
    A5ResultsStd.append([]);
    A5TimesMean.append([]);
    A5TimesStd.append([]);
    for VCount in range(0,MaxV - MinV + 1):
        A5ResultsMean[AlgNo].append([]);
        A5ResultsStd[AlgNo].append([]);
        A5TimesMean[AlgNo].append([]);
        A5TimesStd[AlgNo].append([]);
        A5ResultsMean[AlgNo][VCount] = Mean(A5Results[VCount][AlgNo]);
        A5ResultsStd[AlgNo][VCount] = Std(A5Results[VCount][AlgNo]);
        A5TimesMean[AlgNo][VCount] = Mean(A5Times[VCount][AlgNo]);
        A5TimesStd[AlgNo][VCount] = Std(A5Times[VCount][AlgNo]);

A5ResultsMeanFile = open("A5ResultsMean.txt","w");
A5ResultsStdFile = open("A5ResultsStd.txt","w");
A5TimesMeanFile = open("A5TimesMean.txt","w");
A5TimesStdFile = open("A5TimesStd.txt","w");

for AlgNo in range(5):
    for VCount in range(0,MaxV - MinV + 1):
        A5ResultsMeanFile.write(str(A5ResultsMean[AlgNo][VCount]));
        A5ResultsStdFile.write(str(A5ResultsStd[AlgNo][VCount]));
        A5TimesMeanFile.write(str(A5TimesMean[AlgNo][VCount]));
        A5TimesStdFile.write(str(A5TimesStd[AlgNo][VCount]));
        if VCount != MaxV - MinV:
            A5ResultsMeanFile.write(',');
            A5ResultsStdFile.write(',');
            A5TimesMeanFile.write(',');
            A5TimesStdFile.write(',');
    A5ResultsMeanFile.write('\n');
    A5ResultsStdFile.write('\n');
    A5TimesMeanFile.write('\n');
    A5TimesStdFile.write('\n');

A5ResultsMeanFile.close();
A5ResultsStdFile.close();
A5TimesMeanFile.close();
A5TimesStdFile.close();
