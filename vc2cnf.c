#include <stdlib.h>

int IsGraphInit = 0;
unsigned int n, ECnt, *E1, *E2;

int IsVC2CNFInit = 0;
unsigned int k;

#define GetAtomNo(N,K) (K + N*k + 1)

void InitGraph (unsigned int _V_,   unsigned int _ECnt_,
                unsigned int* _E1_, unsigned int* _E2_)
{
    unsigned int i;
    n = _V_;
    ECnt = _ECnt_;
    E1 = (unsigned int *) (malloc(sizeof(unsigned int)*ECnt));
    E2 = (unsigned int *) (malloc(sizeof(unsigned int)*ECnt));
    for (i = 0; i < ECnt; i++)
    {
        E1[i] = _E1_[i];
        E2[i] = _E2_[i];
    }
    IsGraphInit = 1;
}

void InitVC2CNF (int _k_)
{
    k = _k_;
    IsVC2CNFInit = 1;
}


void UnInitGraph (void)
{
    free(E1);
    free(E2);
    IsGraphInit = 0;
}

void UnInitVC2CNF (void)
{
    IsVC2CNFInit = 0;
}

int GetAtomCnt (void)
{
    return n*k;
}

int GetClauseCnt (void)
{
    return (k + n*k*(k - 1)/2 + k*n*(n - 1)/2 + ECnt);
}

void GetClauseLengthes(int* ClauseLengthes)
{
    unsigned int idx;
    for (idx = 0; idx < k; idx++)
        ClauseLengthes[idx] = n;
    for (idx = k; idx < k + n*k*(k - 1)/2; idx++)
        ClauseLengthes[idx] = 2;
    for (   idx = k + n*k*(k - 1)/2;
            idx < k + n*k*(k - 1)/2 + k*n*(n - 1)/2; idx++)
        ClauseLengthes[idx] = 2;
    for (   idx = k + n*k*(k - 1)/2 + k*n*(n - 1)/2; 
            idx < k + n*k*(k - 1)/2 + k*n*(n - 1)/2 + ECnt; idx++)
        ClauseLengthes[idx] = 2*k;
}

void MakeCNF(int** Clauses)
{
    unsigned int idx = 0;
    unsigned int i,m,p,q,j;
    unsigned int l, ENo;
    
    for (i = 0; i < k; i++)
    {
        for (l = 0; l < n; l++)
            Clauses[idx][l] = GetAtomNo(l,i) << 1;
        idx++;
    }
    for (m = 0; m < n; m++)
        for (q = 1; q < k; q++)
            for (p = 0; p < q; p++)
            {
                Clauses[idx][0] = (GetAtomNo(m,p) << 1) | 0x01;
                Clauses[idx][1] = (GetAtomNo(m,q) << 1) | 0x01;;
                idx++;
            }
    for (m = 0; m < k; m++)
        for (q = 1; q < n; q++)
            for (p = 0; p < q; p++)
            {
                Clauses[idx][0] = (GetAtomNo(p,m) << 1) | 0x01;
                Clauses[idx][1] = (GetAtomNo(q,m) << 1) | 0x01;;
                idx++;
            }
    for (ENo = 0; ENo < ECnt; ENo++)
    {
        i = E1[ENo];
        j = E2[ENo];
        for (l = 0; l < k; l++)
            Clauses[idx][l] = GetAtomNo(i,l) << 1;
        for (l = k; l < 2*k; l++)
            Clauses[idx][l] = GetAtomNo(j,l-k) << 1;
        idx++;
    }
}
