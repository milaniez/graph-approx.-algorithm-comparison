#ifndef _VC2CNF_INCLUDED_
#define _VC2CNF_INCLUDED_

void InitGraph (unsigned int _V_, unsigned int _ECnt_, unsigned int* _E1_, unsigned int* _E2_);
void InitVC2CNF (int _k_);

void UnInitGraph (void);
void UnInitVC2CNF (void);

int GetAtomCnt (void);
int GetClauseCnt (void);
void GetClauseLengthes (int* ClauseLengthes);

void MakeCNF(int** Clauses);

#endif
