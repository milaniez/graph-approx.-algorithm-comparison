all: assignment

clean:
	-@rm a5-ece650
	-@rm *.o

assignment:
	g++ -c mygraph.c vc2cnf.c
	g++ a5-ece650.c -o a5-ece650 mygraph.o vc2cnf.o -L. -lsat -lpthread -lrt
